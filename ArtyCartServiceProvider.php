<?php

namespace Arty4\Cart;

use Illuminate\Support\ServiceProvider;

class ArtyCartServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . 'src/database/migrations');

        $this->publishes([
            __DIR__ . 'src/database/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
