<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->string('uid', 255)->unique()->nullable();
            $table->integer('user_id')->nullable();
            $table->string('user_uid', 255)->nullable();
            $table->integer('status_id')->nullable();
            $table->decimal('total', 19, 4)->nullable();
            $table->text('information')->nullable();
            $table->text('items')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
