<?php

namespace Arty4\Cart\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cart
 *
 * @package App\Models
 * @version April 7, 2021, 12:17 pm UTC
 * @property integer $user_id
 * @property string $user
 * @property integer $product_id
 * @property string $product
 * @property number $price
 * @property integer $quantity
 * @property string $url
 * @property string $picture_web
 * @property string $picture_src
 * @property string $picture
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Database\Factories\CartFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart newQuery()
 * @method static \Illuminate\Database\Query\Builder|Cart onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart wherePictureSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart wherePictureWeb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Cart withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Cart withoutTrashed()
 * @mixin Model
 */
class Cart extends Model
{
    use SoftDeletes;

    use HasFactory;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'nullable|integer',
        'user' => 'nullable|string',
        'product_id' => 'nullable|integer',
        'product' => 'nullable|string',
        'price' => 'nullable|numeric',
        'quantity' => 'nullable|integer',
        'url' => 'nullable|string',
        'picture_web' => 'nullable|string|max:255',
        'picture_src' => 'nullable|string|max:255',
        'picture' => 'nullable|string|max:65535',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
    public $table = 'carts';
    public $fillable = [
        'user_id',
        'user',
        'product_id',
        'product',
        'price',
        'quantity',
        'url',
        'picture_web',
        'picture_src',
        'picture'
    ];
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'user' => 'string',
        'product_id' => 'integer',
        'product' => 'string',
        'price' => 'decimal:4',
        'quantity' => 'integer',
        'url' => 'string',
        'picture_web' => 'string',
        'picture_src' => 'string',
        'picture' => 'string'
    ];


}
